package kodlamaio.hrms.business.abstracts;

import java.util.List;

import kodlamaio.hrms.entities.concrates.HrmsPersonel;

public interface HrmsPersonelService {
		List<HrmsPersonel> getAll();
}
