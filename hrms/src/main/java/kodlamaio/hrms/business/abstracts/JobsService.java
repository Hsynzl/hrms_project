package kodlamaio.hrms.business.abstracts;

import java.util.List;

import kodlamaio.hrms.entities.concrates.Jobs;

public interface JobsService {
	List<Jobs> getAll();
}
