package kodlamaio.hrms.business.abstracts;

import java.util.List;

import kodlamaio.hrms.entities.concrates.Unemployed;

public interface UnemployedService {
		List<Unemployed> getAll();
}
