package kodlamaio.hrms.business.abstracts;

import java.util.List;

import kodlamaio.hrms.entities.concrates.Employers;

public interface EmployersService {
		List<Employers> getAll();
}
