package kodlamaio.hrms.entities.concrates;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="job_title")
public class Jobs {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="title")
	private String jobTitle;
	
	public Jobs() {}
	
	public Jobs(int id, String jobTitle) {
		super();
		this.id = id;
		this.jobTitle = jobTitle;
	}
}
