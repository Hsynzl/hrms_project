package kodlamaio.hrms.entities.concrates;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name="employers")
@Data
public class Employers extends Users {
	
	@Id
	@Column(name="Id")
	private int id;
	
	@Column(name="company_name")
	private String companyName;

	@Column(name="web_adress")
	private String companyWebSite;
	
	@Column(name="phone_number")
	private String companyPhone;
	
	public Employers() {}

	public Employers(int id, String companyName, String companyWebSite, String companyPhone) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.companyWebSite = companyWebSite;
		this.companyPhone = companyPhone;
	}

	

	
}
