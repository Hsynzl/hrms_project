package kodlamaio.hrms.entities.concrates;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@Table(name="candidates")
public class Users {
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="email")
	private String mail;
	
	@Column(name="password")
	private String pass;
	
	public Users() {}
	
	public Users(int id, String mail, String pass) {
		super();
		this.id = id;
		this.mail = mail;
		this.pass = pass;
	}


	
}
