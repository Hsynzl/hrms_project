package kodlamaio.hrms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrmsApplication {
	//Huseyin Ozlu
	public static void main(String[] args) {
		SpringApplication.run(HrmsApplication.class, args);
	}

}
